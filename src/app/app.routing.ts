import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LoadersCssModule } from 'angular2-loaders-css';

const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: 'home',loadChildren: 'app/home/home.module#HomeModule'},
  { path: '**', redirectTo: '', pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
