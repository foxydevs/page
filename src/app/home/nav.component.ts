import { Component, OnInit, ViewChild, Input } from '@angular/core';
import { Router, ActivatedRoute } from "@angular/router";
import { ProductosService } from './_services/productos.service';
import { TipoProductosService } from './_services/tipo-productos.service';

import { NotificationsService } from 'angular2-notifications';
import { UsersService } from "./_services/users.service";
import { CarritoComponent } from "./carrito/carrito.component";
import 'rxjs/add/observable/fromEvent'

declare var $: any

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.css']
})
export class NavComponent implements OnInit {

  user=localStorage.getItem('currentUser');
  firstname=localStorage.getItem('currentFirstName');
  lastname=localStorage.getItem('currentLastName');
  picture=localStorage.getItem('currentPicture');
  id=localStorage.getItem('currentId');
  type=localStorage.getItem('currentType');
  state=localStorage.getItem('currentState');
  rol=localStorage.getItem('currentRol');
  idRol=+localStorage.getItem('currentRolId');
  click:boolean
  notifications:any = []
  nNotifications:number = 0;
  modulos:any
  modulosOcultos:any
  agregados: any[] = [];
  categorias: any;

  constructor(
    private _service: NotificationsService,
    private route: ActivatedRoute,
    private router: Router,
    private UsersService: UsersService,
    private mainService: ProductosService,
    private parentService: TipoProductosService
  ) { }

  ngOnInit() {


    this.cargarCarrito();
   if(this.state=='21'){
      $('#passwordModal').modal();
    }
    // if(this.type=="tutor"){
    //   this.cargarNotifications();
    // }
    this.cargarParents();
    // console.log(this.idRol);

  }
  cargarCarrito(){
    let datos = localStorage.getItem('carrito');
    // console.log(datos);
    if (datos) {
      //localStorage.getItem('carrito')
    this.agregados = JSON.parse(datos);
    }
  }
  cargarParents(){
    this.parentService.getAll()
                      .then(response => {
                        this.categorias=response;
                      }).catch(error => {

                      })
  }
  cargarLogin(data){
    if(data){
      this.user=localStorage.getItem('currentUser');
      this.firstname=localStorage.getItem('currentFirstName');
      this.lastname=localStorage.getItem('currentLastName');
      this.picture=localStorage.getItem('currentPicture');
      this.id=localStorage.getItem('currentId');
      this.type=localStorage.getItem('currentType');
      this.state=localStorage.getItem('currentState');
      this.rol=localStorage.getItem('currentRol');
      this.idRol=+localStorage.getItem('currentRolId');
    }else{
      this.user=null;
      this.firstname=null;
      this.lastname=null;
      this.picture=null;
      this.id=null;
      this.type=null;
      this.state=null;
      this.rol=null;
      this.idRol=null;
    }
  }
  permiso(obj:any){
    localStorage.setItem('permisoAgregar',obj.agregar);
    localStorage.setItem('permisoEliminar',obj.eliminar);
    localStorage.setItem('permisoModificar',obj.modificar);
    localStorage.setItem('permisoMostrar',obj.mostrar);

  }

  updatePass(formValue:any){
    $('#Loading').css('display','block')
    $('#Loading').addClass('in')



    let data = {
      id: this.id,
      old_pass: formValue.old_pass,
      new_pass: formValue.new_pass,
      new_pass_rep: formValue.new_pass_rep
    }
    // console.log(data)
    this.UsersService.updatePass(data)
                      .then(response => {
                        console.clear
                        this.create('Contraseña cambiada exitosamente')
                        $('#Loading').css('display','none')
                        $("#passwordModal .close").click();
                        $('#pass-form')[0].reset()
                        localStorage.setItem('currentState',response.state);
                      }).catch(error => {
                        console.clear
                        this.createError(error)
                        $('#Loading').css('display','none')
                      })

  }
  logout() {
    // remove user from local storage to log user out
    localStorage.removeItem('currentUser');
    localStorage.removeItem('currentEmail');
    localStorage.removeItem('currentFirstName');
    localStorage.removeItem('currentLastName');
    localStorage.removeItem('currentId');
    localStorage.removeItem('currentType');
    this.cargarLogin(false)
    this.router.navigate([`home/inicio`])

  }
  public options = {
               position: ["bottom", "right"],
               timeOut: 2000,
               lastOnBottom: false,
               animate: "fromLeft",
               showProgressBar: false,
               pauseOnHover: true,
               clickToClose: true,
               maxLength: 200
           };

    create(success) {
                this._service.success('¡Éxito!',success)

    }
    createError(error) {
                this._service.error('¡Error!',error)

    }
    remover(data: any) {
      this.agregados.splice(this.agregados.findIndex(dat => {
        return dat.id == data.id
      }), 1)
    }
    emptyCArt(){
      this.agregados.length=0;
      // this.Navegador.agregados.length=0;
      localStorage.setItem('carrito', JSON.stringify(this.agregados));
    }
    agregado(data:any){
      let prueba:any;
      this.agregados.push(data);
      localStorage.setItem('carrito', JSON.stringify(this.agregados));
      // console.log(this.agregados);
      $('#Loading').css('display','none')
          this.create("Se Agrego Al Carrito")
      // console.log(this.agregados.length);
}
}
