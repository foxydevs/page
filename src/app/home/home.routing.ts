import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { NavComponent } from './nav.component';
import { LoginComponent } from './login/login.component';
import { InicioComponent } from './inicio/inicio.component';
import { BuscarComponent } from './buscar/buscar.component';
import { CategoriaComponent } from './categoria/categoria.component';
import { ProductoComponent } from './producto/producto.component';
import { RecoveryComponent } from './recovery/recovery.component';
import { RegisterComponent } from './register/register.component';
import { DetalleproductoComponent } from './detalleproducto/detalleproducto.component';
import { CategoriaProductosComponent } from "./categoria-productos/categoria-productos.component";
import { CarritoComponent } from './carrito/carrito.component';
import { PagoComponent } from './pago/pago.component';
import { ComprobanteComponent } from './comprobante/comprobante.component';

const routes: Routes = [
  { path: '', redirectTo: 'inicio', pathMatch: 'full' },
  { path: '', component: NavComponent, children: [
    { path: 'login', component: LoginComponent },
    { path: 'inicio', component: InicioComponent },
    { path: 'buscar', component: BuscarComponent },
    { path: 'buscar/:fac', component: BuscarComponent },
    { path: 'categoria', component: CategoriaComponent },
    { path: 'producto', component: ProductoComponent },
    { path: 'recovery', component: RecoveryComponent },
    { path: 'register', component: RegisterComponent },
    { path: 'producto/detalleproducto/:id', component: DetalleproductoComponent },
    { path: 'producto/categoria/:id', component: CategoriaProductosComponent },
    { path: 'carrito', component: CarritoComponent },
    { path: 'pago', component: PagoComponent },
    { path: 'comprobante/:token/:ern', component: ComprobanteComponent }
  ]},
  { path: '**', redirectTo: '', pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HomeRoutingModule { }
