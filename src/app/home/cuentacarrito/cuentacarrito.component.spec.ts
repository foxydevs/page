import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CuentacarritoComponent } from './cuentacarrito.component';

describe('CuentacarritoComponent', () => {
  let component: CuentacarritoComponent;
  let fixture: ComponentFixture<CuentacarritoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CuentacarritoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CuentacarritoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
