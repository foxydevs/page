import { Component, Input, OnInit } from '@angular/core';
import {ActivatedRoute, Params, Router } from '@angular/router';
import { Location } from '@angular/common';

import { DetalleproductoService } from './../_services/detalleproducto.service';
import { NotificationsService } from 'angular2-notifications';
import { Subject } from 'rxjs/Rx';
import 'rxjs/add/operator/switchMap';

declare var $: any;

@Component({
  selector: 'app-detalleproducto',
  templateUrl: './detalleproducto.component.html',
  styleUrls: ['./detalleproducto.component.css']
})

export class DetalleproductoComponent implements OnInit {
  @Input() id:any;
  Table:any
  selectedData:any
  Ids:any = '';
  tipo:any
  public _id: number;
  public search: any;

  constructor(
    private _service: NotificationsService,
    private mainService: DetalleproductoService,
    private route: ActivatedRoute,
    private location:Location,
    private router:Router
  ) { }

  ngOnInit() {
    this.route.params
    .switchMap((params: Params) => (params['id']))
    .subscribe(response => {
                      this.Ids+=response
                  });
                  //console.log(this.Ids);

    this.cargarSingle(this.Ids)
    this.cargarAll();
  }

  cargarAll(){
    $('#Loading').css('display','block')
    $('#Loading').addClass('in')
    this.mainService.getAll(this.Ids)
                      .then(response => {
                        this.Table = response
                        console.clear
                        $('#Loading').css('display','none')
                      }).catch(error => {
                        console.clear
                        this.createError(error)
                      })
  }

  cargarSingle(id:number){
    $('#Loading').css('display','block')
    $('#Loading').addClass('in')
    this.mainService.getSingle(id)
                      .then(response => {
                        this.selectedData = response;
                        console.log(response);

                        $('#Loading').css('display','none')
                      }).catch(error => {
                        console.clear
                        this.createError(error)
                      })
  }


  public options = {
    position: ['bottom', 'right'],
    timeOut: 2000,
    lastOnBottom: false,
    animate: 'fromLeft',
    showProgressBar: false,
    pauseOnHover: true,
    clickToClose: true,
    maxLength: 200
  };

  create(success){
    this._service.success('¡Éxito!', success);
  }
  createError(error){
    this._service.error('¡Error!', error);
  }
}
