import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { Location } from '@angular/common';

import { TipoProductosService } from './../_services/tipo-productos.service';
import { NotificationsService } from 'angular2-notifications';
import { Subject } from 'rxjs/Rx';
import 'rxjs/add/operator/switchMap';

//Declarar valiable any typescript

declare var $: any;

//Componente sobre el cual se trabajara
@Component({
  selector: 'app-categoria',
  templateUrl: './categoria.component.html',
  styleUrls: ['./categoria.component.css']
})

//Aqui se colocan los metodos que se van a utilizar
export class CategoriaComponent implements OnInit {
  Table: any;
  public _id: number;
  public search: any;
  selectedData: any;
  Id:any = '';
  categorylist: any;

//Servicio el cual se va a trabajar
  constructor(
    private _service: NotificationsService,
    private mainService: TipoProductosService,
    private route: ActivatedRoute,
    private location: Location,
    private router: Router
  ) { }

  //Llamar los metodos que se van a utilizar
  ngOnInit() {
    this.cargarAll();
  }

  cargarAll(){
   $('#Loading').css('display','block')
   $('#Loading').addClass('in')
   this.mainService.getAll()
                        .then(response => {
                          this.categorylist = response;
                          console.log(response);
                          
                          $('#Loading').css('display','none')
                        }).catch(error => {
                          console.clear     
                          $('#Loading').css('display','none')
                          this.createError(error) 
                        })
      
    }

    public options = {
        position: ['bottom', 'right'],
        timeOut: 2000,
        lastOnBottom: false,
        animate: 'fromLeft',
        showProgressBar: false,
        pauseOnHover: true,
        clickToClose: true,
        maxLength: 200
    };

    create(success) {
          this._service.success('¡Éxito!', success);

    }
    createError(error) {
          this._service.error('¡Error!', error);

    }
}
