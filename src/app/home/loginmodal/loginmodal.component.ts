import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from "@angular/router";

import { AuthService } from "./../_services/auth.service";
import { NavComponent } from "./../nav.component";

import { NotificationsService } from 'angular2-notifications';

declare var $: any

@Component({
  selector: 'app-loginmodal',
  templateUrl: './loginmodal.component.html',
  styleUrls: ['./loginmodal.component.css']
})
export class LoginmodalComponent implements OnInit {
auth:any
closeResult: string

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private Nav:NavComponent,
    private authenticationService: AuthService,
    private _service: NotificationsService
  ) { }

  public options = {
    position: ["bottom", "right"],
    timeOut: 3000,
    showProgressBar: false,
    pauseOnHover: true,
    clickToClose: true,
    lastOnBottom: false,
    preventDuplicates: true,
    animate: "scale",
    maxLength: 400
  };

  create(text) {
    this._service.error('Error!',text)

}
login(formValue:any){
  //  console.log(`user: ${formValue.username} pass: ${formValue.password}`)

   $('#Loading').css('display','block')
   $('#Loading').addClass('in')

    this.authenticationService.Authentication(formValue)
      .then(response => {
        this.auth = response
        if(response.estado>0 && response.cliente){
          // console.log(response.username)
          // console.log(response)
          let type:string = null;
          localStorage.setItem('currentUser', response.username);
          localStorage.setItem('currentEmail', response.email);
          localStorage.setItem('currentFirstName', ((response.empleados?response.empleados.nombre:'')));
          localStorage.setItem('currentLastName', ((response.empleados?response.empleados.apellido:'')));
          localStorage.setItem('currentId', response.id);
          localStorage.setItem('currentPicture', response.picture);
          localStorage.setItem('currentState', response.estado);
          localStorage.setItem('currentRol', response.roles.descripcion);
          localStorage.setItem('currentRolId', response.rol);
          if(response.cliente){
            localStorage.setItem('currentClienteId', response.cliente);
          }
          $('#abrirLogin').modal('hide')
          this.Nav.cargarLogin(true)
          setTimeout(() => {
          $('#Loading').css('display','none')
          this.router.navigate([`home/pago`])
          }, 1000);
        }else{
          $('#Loading').css('display','none')
          this.create("Su usuario se encuentra deshabilitado temporalmente")
        }
      }).catch(error => {
        console.clear
        $('#Loading').css('display','none')
        if(error.status==401){
          this.create("Usuario o contraseña incorrectos");
        }else{
          this.create(error)
        }

      })


  }
  ngOnInit() {
  }

}
