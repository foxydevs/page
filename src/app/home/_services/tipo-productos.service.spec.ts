import { TestBed, inject } from '@angular/core/testing';

import { TipoProductosService } from './tipo-productos.service';

describe('TipoProductosService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [TipoProductosService]
    });
  });

  it('should be created', inject([TipoProductosService], (service: TipoProductosService) => {
    expect(service).toBeTruthy();
  }));
});
