import { TestBed, inject, ComponentFixture } from '@angular/core/testing';

import { DetalleproductoService } from './detalleproducto.service';

describe('ProductosService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [DetalleproductoService]
    });
  });

  it('should be created', inject([DetalleproductoService], (service: DetalleproductoService) => {
    expect(service).toBeTruthy();
  }));
});
