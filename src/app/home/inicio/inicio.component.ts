import { Component, OnInit } from '@angular/core';
import { NotificationsService } from 'angular2-notifications';
import { TipoProductosService } from "../_services/tipo-productos.service";
import { NavComponent } from './../nav.component';

declare var $: any

@Component({
  selector: 'app-inicio',
  templateUrl: './inicio.component.html',
  styleUrls: ['./inicio.component.css']
})
export class InicioComponent implements OnInit {
  productosGeneral:any
  agregados: any[] = []
  constructor(
    private _service: NotificationsService,
    private Navegador: NavComponent,
    private mainService:TipoProductosService,

  ) { }

  ngOnInit() {

    let datos = localStorage.getItem('carrito');
    // console.log(datos);
    if(datos){
    //localStorage.getItem('carrito')
    this.Navegador.agregados = JSON.parse(datos);
    this.agregados = JSON.parse(datos);
    }
    setTimeout(() => {
      $("#owl-demo").owlCarousel({
        items : 2,
        lazyLoad : false,
        autoPlay : true,
        navigation : true,
        navigationText :  true,
        pagination : false,
      });

      $("#flexiselDemo2").flexisel({
        visibleItems: 1,
        animationSpeed: 1000,
        autoPlay: true,
        autoPlaySpeed: 5000,
        pauseOnHover: true,
        enableResponsiveBreakpoints: true,
          responsiveBreakpoints: {
            portrait: {
              changePoint:480,
              visibleItems: 1
            },
            landscape: {
              changePoint:640,
              visibleItems: 1
            },
            tablet: {
              changePoint:768,
              visibleItems: 1
            }
          }
        });
    }, 500);
      this.productos();
  }

  productos(){
      this.mainService.getAllWithProds()
                        .then(response => {
                          this.productosGeneral = response
                          // console.log(response);

                        }).catch(error => {
                          this.createError(error)
                        })
  }


  public options = {
               position: ["bottom", "right"],
               timeOut: 2000,
               lastOnBottom: false,
               animate: "fromLeft",
               showProgressBar: false,
               pauseOnHover: true,
               clickToClose: true,
               maxLength: 200
           };

    create(success) {
                this._service.success('¡Éxito!',success)

    }
    createError(error) {
                this._service.error('¡Error!',error)

    }

    agregado(Navegador:any){

      this.agregados.push(Navegador);
      this.Navegador.agregados.push(Navegador);
      localStorage.setItem('carrito', JSON.stringify(this.agregados));
      console.log(this.agregados);
      $('#Loading').css('display','none')
          this.create("Se Agrego Al Carrito")
      //console.log(this.agregados.length);
      //location.reload();
    }
}
