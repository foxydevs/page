import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { Location } from '@angular/common';

import { ProductosService } from './../_services/productos.service';
import { NotificationsService } from 'angular2-notifications';
import { AuthService } from './../_services/auth.service';
import { NavComponent } from './../nav.component';
import { Subject } from 'rxjs/Rx';
import 'rxjs/add/operator/switchMap';

declare var $: any;

@Component({
  selector: 'app-carrito',
  templateUrl: './carrito.component.html',
  styleUrls: ['./carrito.component.css']
})
export class CarritoComponent implements OnInit {
  @Input() id:any;
  Table: any;
  clientes: any;
  formas: any;
  Idn: any = '';
  public _id: number;
  public search: any;
  agregados: any[] = [];

  constructor(
    private _service: NotificationsService,
    private mainService: ProductosService,
    private route: ActivatedRoute,
    private location: Location,
    private router: Router,
    private authenticationService: AuthService,
    private Navegador: NavComponent

  ) { }

  ngOnInit() {

    let datos = localStorage.getItem('carrito');
    // console.log(datos);
    if(datos){
      //localStorage.getItem('carrito')
      this.agregados = JSON.parse(datos);
      console.log(this.agregados);

    }
  }

  public options = {
    position: ['bottom', 'right'],
    timeOut: 2000,
    lastOnBottom: false,
    animate: 'fromLeft',
    showProgressBar: false,
    pauseOnHover: true,
    clickToClose: true,
    maxLength: 200
  };

  create(success){
    this._service.success('¡Éxito!', success);
  }
  createError(error){
    this._service.error('¡Error!', error);
  }

  remover(data: any) {
    this.Navegador.remover(data);
    this.agregados.splice(this.agregados.findIndex(dat => {
      return dat.id == data.id
    }), 1)
    localStorage.setItem('carrito', JSON.stringify(this.agregados));

    $('#Loading').css('display', 'none')
    this.create("Producto Eliminado");
  }

  pagar(){
    let currentUser = localStorage.getItem('currentUser')
    if(currentUser){
      //console.log('Logueado');
      this.router.navigate(['/home/pago'])
      return false;
    }
    else{
    //console.log('No Lo Esta');
    //this.router.navigate([`/home/login`])
    $('#abrirLogin').modal('show')
    return false;
    }
  }
}
