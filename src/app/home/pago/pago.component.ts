import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { Location } from '@angular/common';

import { ProductosService } from './../_services/productos.service';
import { NotificationsService } from 'angular2-notifications';
import { AuthService } from '../_services/auth.service';
import { UsersService } from './../_services/users.service';
import { ClientesService } from './../_services/clientes.service';
import { VentasService } from './../_services/ventas.service';
import { Subject } from 'rxjs/Rx';
import 'rxjs/add/operator/switchMap';
import { forEach } from '@angular/router/src/utils/collection';
import { log } from 'util';
import { element } from 'protractor';
import { NavComponent } from './../nav.component';

declare var $: any;

@Component({
  selector: 'app-pago',
  templateUrl: './pago.component.html',
  styleUrls: ['./pago.component.css']
})
export class PagoComponent implements OnInit {
  @Input() id:any;
  Table: any;
  Idn: any = '';
  public _id: number;
  public search: any;
  selectedData: any[];
  clientes: any;
  formas: any;
  obtenerForma: any;
  obtenerDireccion: any;
  obtenerCliente: any;
  agregados: any[] = [];
  inventario: any;
  numero: any; //Variable para Numero Aleatorio del Tracking
  sumas: any;
  fechaHoy : any;
  clienteId = localStorage.getItem('currentId')
  formaPagoId = localStorage.getItem('currentId')
  cli:any ={
    direccion: " ",
    fecha: "",
    nit: "",
    nombre: "",
    tipo: 1,
    total: "",
    usuario: 1
  }

  deta:any = {
    detalle:{
    cantidad: 0,
    codigo: "1",
    descripcion: " ",
    id: 0,
    marcaDes: "",
    nombre: " ",
    precioClienteEs: 0,
    precioDistribuidor: " ",
    precioVenta: " ",
    producto: " ",
    rId:0,
    subtotal: 0,
    tipo: 0
  }
}

selectedEntryDireccion:any
selectedEntryForma:any
  constructor(
    private _service: NotificationsService,
    private mainService: VentasService,
    private route: ActivatedRoute,
    private location: Location,
    private router: Router,
    private authenticationService: AuthService,
    private UsersService: UsersService,
    private clientService: ClientesService,
    private Navegador: NavComponent
  ) { }

  ngOnInit() {
  let date = new Date;
  let month = date.getMonth()+1;
  let month2;
  let dia = date.getDate();
  let dia2;
  if(month < 10){ month2 = '0' + month}
  else { month2 = month }
  if(dia < 10) {dia2 = '0' + dia}
  else{ dia2 = dia }
  this.fechaHoy = date.getFullYear()+'-'+month2+'-'+dia2

    let datos = localStorage.getItem('carrito')
    // console.log(datos);
    if(datos)
    {
      this.agregados = JSON.parse(datos);
    }

    this.cargarAllClientes();
    this.cargarFormasPago();
    this.sumatoriaTotal();
    this.cargarClientes();

  }
  cargarAll(){
    $('#Loading').css('display', 'block')
    $('#Loading').addClass('in')
    this.mainService.getAll()
                        .then(response => {
                          this.Table = response;
                          // console.log(response);

                          $('#Loading').css('display', 'none')
                        }).catch(error => {
                          console.clear
                          $('#Loading').css('display', 'none')
                          this.createError(error)
                        })
  }

  generarTracking(tam:number)
  {
    let i:number
    var caracteres = "123456789bcdefghijkmnpqrtuvwxyz123456789BCDEFGHIJKLMNPQRTUVWXYZ12346789";
    var contraseña = "";
    for (i=0; i<tam; i++) contraseña += caracteres.charAt(Math.floor(Math.random()*caracteres.length));
    // console.log(contraseña)
    this.numero = contraseña
    return contraseña;
  }

  cargarAllClientes() {
    if (this.clienteId) {
    $('#Loading').css('display', 'block')
    $('#Loading').addClass('in')
    this.UsersService.getDireccionesCliente(+this.clienteId)
      .then(response => {
        this.clientes = response;
        this.obtenerCliente = this.clienteId

        if(this.clientes) {
          this.onSelectionChange(this.clientes[0],true);
        }
        // console.log(response);
        $('#Loading').css('display', 'none')
      }).catch(error => {
        console.clear
        $('#Loading').css('display', 'none')
        this.createError(error)
      })
    }
    else{
      console.log('No Hay Direcciones');
    }
  }

  cargarFormasPago() {
    if (this.formaPagoId) {
    $('#Loading').css('display', 'block')
    $('#Loading').addClass('in')
      this.UsersService.getFormasPago(+this.formaPagoId)
      .then(response => {
        this.formas = response;
        this.obtenerForma = this.formaPagoId
        if(this.formas) {
          this.onSelectionChange(this.formas[0],false);
        }
        $('#Loading').css('display', 'none')
      }).catch(error => {
        console.clear
        $('#Loading').css('display', 'none')
        this.createError(error)
        })
    }
    else{
      console.log('No Hay Formas de Pago');
    }
  }
  onSelectionChange(entry,sele) {
    // clone the object for immutability
    if(sele){
      this.selectedEntryDireccion = Object.assign({}, this.selectedEntryDireccion, entry);
    }else{
      this.selectedEntryForma = Object.assign({}, this.selectedEntryForma, entry);
    }
  }
  PagarProductosCarrito(){

    $('#Loading').css('display', 'block')
    $('#Loading').addClass('in')
    let id = localStorage.getItem('currentId')
    let ern = this.generarTracking(8);
    this.agregados.forEach(element => {
        element.subtotal = element.cantidad*element.precioVenta;

    });

    let data=
    {
      cliente:this.Table.id,
      comprobante:this.obtenerForma,
      direccion: this.selectedEntryDireccion.id,
      fecha: this.fechaHoy,
      nit:this.Table.nit,
      nombre: this.Table.nombre,
      total: this.sumas,
      unit_price: this.sumas,
      ern: ern,
      usuario:id,
      formapago: this.selectedEntryForma.id,
      tipo:1,
      detalle: this.agregados
    }
    let order = {
      cantidad:data.detalle.length,
      precio:data.total,
      ern:ern,
      descripcion:`Venta por el total de ${data.detalle.length} articulos un total de Q${data.total} `,
      url:"www.foxylabs.gt"
    }
    this.mainService.pagar(order)
                      .then(response2 =>{
                        this.mainService.create(data)
                                        .then(response => {
                                          $('#Loading').css('display', 'none')
                                          console.log(response2);
                                          this.Navegador.agregados.length=0;
                                          this.agregados.length=0;
                                          localStorage.setItem('carrito', JSON.stringify(this.agregados));
                                          location.href = response2.token
                                        }).catch(error => {
                                          console.clear
                                          $('#Loading').css('display', 'none')
                                          this.createError(error)
                                        })
                      }).catch(error=>{
                        $('#Loading').css('display', 'none')
                        this.createError(error)
                      })


  }


  cargarClientes(){
    $('#Loading').css('display','block')
    $('#Loading').addClass('in')
    let id = localStorage.getItem('currentClienteId')

    this.clientService.getSingle(+id)
                      .then(response => {
                        this.Table = response;

                        $('#Loading').css('display','none')
                        console.clear
                      }).catch(error => {
                        console.clear
                        this.createError(error)
                        $('#Loading').css('display','none')
                      })
  }

  sumatoriaTotal(){
    let suma = 0;
    this.agregados.forEach(element => {
      suma += +element.precioVenta;
      this.deta.cantidad = +element.cantidad
      this.deta.precioClienteEs = +element.precioClienteEs
      this.deta.precioDistribuidor = +element.precioDistribuidor
      this.deta.precioVenta = +element.precioVenta
      this.deta.producto =  +element.producto
      this.deta.subtotal = suma
      this.deta.nombre =
      this.deta.tipo = +this.formaPagoId
    });
    this.sumas = suma;
  }

  public options = {
    position: ['bottom', 'right'],
    timeOut: 2000,
    lastOnBottom: false,
    animate: 'fromLeft',
    showProgressBar: false,
    pauseOnHover: true,
    clickToClose: true,
    maxLength: 200
  };

  create(success) {
    this._service.success('¡Éxito!', success);
  }
  createError(error) {
    this._service.error('¡Error!', error);
  }

}
