import { Component, Input, OnInit, Output } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { Location } from '@angular/common';

import { ProductosService } from './../_services/productos.service';
import { NotificationsService } from 'angular2-notifications';
import { TipoProductosService } from './../_services/tipo-productos.service';
import { NavComponent } from './../nav.component';
import { Subject } from 'rxjs/Rx';
import 'rxjs/add/operator/switchMap';
import { Http } from '@angular/http';

declare var $: any;

@Component({
  selector: 'app-categoria-productos',
  templateUrl: './categoria-productos.component.html',
  styleUrls: ['./categoria-productos.component.css']
})
export class CategoriaProductosComponent implements OnInit {
  @Input() id:any;
  Table : any;
  selectedData:any[];
  Idn:any = '';
  public _id: number;
  public search: any;
  productslist: any;
  agregados: any[] = [];
  combo: any;

  constructor(

  private _http: Http,
  private _service: NotificationsService,
  private mainService: ProductosService,
  private parentService: TipoProductosService,
  private route: ActivatedRoute,
  private location: Location,
  private router: Router,
  private Navegador: NavComponent
  ) { }

  ngOnInit() {
    if(this.route.params){
      this.route.params
      .switchMap((params: Params) => (params['id']))
      .subscribe(response => {
                        this.Idn+=response
                    });
                    // console.log(this.Idn);
                 }

    this.cargarAll(this.Idn);
    this.getAllCategorias();
    this.productslist = this.Table;

    let datos = localStorage.getItem('carrito');
    // console.log(datos);
    if(datos){
    //localStorage.getItem('carrito')
    this.Navegador.agregados = JSON.parse(datos);
    this.agregados = JSON.parse(datos);
    }
  }



 cargarAll(id){
    this.Idn=id;
    $('#Loading').css('display', 'block')
    $('#Loading').addClass('in')
    this.mainService.getAllCategorias(id)
                        .then(response => {
                          this.productslist = response;
                          console.log(response);

                          $('#Loading').css('display', 'none')
                        }).catch(error => {
                          console.clear
                          $('#Loading').css('display', 'none')
                          this.createError(error)
                        })
  }

  getAllCategorias(){
    $('#Loading').css('display', 'block')
    $('#Loading').addClass('in')
    this.parentService.getAll()
                        .then(response => {
                          this.combo = response;
                          // console.log(response);

                          $('#Loading').css('display', 'none')
                        }).catch(error => {
                          console.clear
                          $('#Loading').css('display', 'none')
                          this.createError(error)
                        })
  }

  cargarSingle(id:number){
    $('#Loading').css('display','block')
    $('#Loading').addClass('in')
    this.mainService.getSingle(id)
                      .then(response => {
                        this.selectedData = response;
                        $('#Loading').css('display','none')
                      }).catch(error => {
                        console.clear
                        this.createError(error)
                      })
  }

  public options = {
    position: ['bottom', 'right'],
    timeOut: 2000,
    lastOnBottom: false,
    animate: 'fromLeft',
    showProgressBar: false,
    pauseOnHover: true,
    clickToClose: true,
    maxLength: 200
  };

    create(success){
      this._service.success('¡Éxito!', success);
    }
    createError(error){
      this._service.error('¡Error!', error);
    }
    agregado(Navegador:any){

      this.agregados.push(Navegador);
      this.Navegador.agregados.push(Navegador);
      localStorage.setItem('carrito', JSON.stringify(this.agregados));
      console.log(this.agregados);
      $('#Loading').css('display','none')
          this.create("Se Agrego Al Carrito")
      //console.log(this.agregados.length);
      //location.reload();
    }
}
