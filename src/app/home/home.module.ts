import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { LoadersCssModule } from 'angular2-loaders-css';
import { SimpleNotificationsModule } from 'angular2-notifications';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { CarouselModule} from 'angular2-carousel';
import { DataTableModule } from 'angular2-datatable';

import { AuthService } from './_services/auth.service';

import { HomeRoutingModule } from './home.routing';
import { NavComponent } from './nav.component';

import { UsersService } from './_services/users.service';
import { ClientesService } from './_services/clientes.service';
import { ProductosService } from './_services/productos.service';
import { TipoProductosService } from './_services/tipo-productos.service';
import { DetalleproductoService } from './_services/detalleproducto.service';
import { VentasService } from './_services/ventas.service';

import { LoginComponent } from './login/login.component';
import { LoaderComponent } from './loader/loader.component';
import { BuscarComponent } from './buscar/buscar.component';
import { CategoriaComponent } from './categoria/categoria.component';
import { ProductoComponent } from './producto/producto.component';
import { InicioComponent } from './inicio/inicio.component';
import { RecoveryComponent } from './recovery/recovery.component';
import { RegisterComponent } from './register/register.component';
import { DetalleproductoComponent } from './detalleproducto/detalleproducto.component';
import { CategoriaProductosComponent } from './categoria-productos/categoria-productos.component';
import { CarritoComponent } from './carrito/carrito.component';
import { PagoComponent } from './pago/pago.component';
import { LoginmodalComponent } from './loginmodal/loginmodal.component';
import { CuentacarritoComponent } from './cuentacarrito/cuentacarrito.component';
import { ComprobanteComponent } from './comprobante/comprobante.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    SimpleNotificationsModule.forRoot(),
    LoadersCssModule,
    Ng2SearchPipeModule,
    CarouselModule,
    HomeRoutingModule,
    DataTableModule
  ],
  declarations: [
    NavComponent,
    LoginComponent,
    LoaderComponent,
    BuscarComponent,
    CategoriaComponent,
    ProductoComponent,
    InicioComponent,
    RegisterComponent,
    RecoveryComponent,
    DetalleproductoComponent,
    CategoriaProductosComponent,
    CarritoComponent,
    PagoComponent,
    LoginmodalComponent,
    CuentacarritoComponent,
    ComprobanteComponent
  ],
  providers: [
    AuthService,
    ClientesService,
    UsersService,
    TipoProductosService,
    ProductosService,
    VentasService,
    DetalleproductoService
  ]
})
export class HomeModule { }
